# cterasdk-gui

unofficial wxPython implementations with cterasdk to allow for server initialization, portal upgrade, and other tasks.

## ctera management agent

cma.py is used for deployment of new portal servers.
- Log file will be generated at cma.log

## ctera upgrade utility
portal_upgrade_tool.py is used to upgrade the CTERA portal software in the appropriate order.
- Log file will be generated at portal_upgrade.log

