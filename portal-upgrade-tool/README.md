# cterasdk-gui

unofficial wxPython implementation with cterasdk to allow for server initialization, portal configuration, and file uploads.


## ctera upgrade utility
portal_upgrade_tool.py is used to upgrade the CTERA portal software in the appropriate order.
- Log file will be generated at portal_upgrade.log
- Windows .exe built with: `pyinstaller.exe --hidden-import=pkg_resources.py2_warn --onefile -w 'portal_upgrade_tool.py'`

### How to use
* Requires network access to all portal servers on port 22 from client workstation running the tool.
* Requires network access to the application portal server on port 443 from client workstation running the tool.
* The root (slash) and /usr/local/lib/ctera directories must have at least 2.5GB and 5GB of available space, respectively, or the tool will not allow the upgrade.
* You can re-run the pre_check option if your upgrade was not allowed due to space issues, after making the appropriate space available.

#### Setting up the tool
1. Run agent .exe or .py (if installed)
    * This will auto tail the portal_upgrade.log file for monitoring the status
    * grep for 'WARNING' for less verbose logs
2. Enter portal application server IP address  (portal DNS name is also supported here)
3. Enter CTERA portal admin user name and password
4. Enter root user password for the application server used in step 2
5. Select portal image file
6. Select portal version file
7. Change upload path (if necessary)

#### Running the tool
If your portal servers do not share a root password, you must first run the '**setup_auth**' action before running anything else.  If they do share a root password, this step is optional.


1. Under 'Actions', select '**pre_check**', click 'RUN' button, wait for completion in logs
    * The tool will load a list of servers into the list field in this format (IP/name/role/rootDirSpaceAvailable/dataDirSpaceAvailable/imageVersion/portalVersion)
2. Under 'Actions', select '**multi_server_upload**', click 'RUN' button, wait for completion in logs
    * This will start uploads of the portal image and portal version to each of the portals servers from the client machine
3. Under 'Actions', select '**unpack_images**', click 'RUN' button, wait for completion in logs
    * This will extract the tar archive of the portal image
4. Under 'Actions', select '**upgrade_db_image**', click 'RUN' button, wait for server reboot completion in logs
    * This will run the portal image upgrade for the main DB and reboot
5. Under 'Actions', select '**upgrade_db_image**', click 'RUN' button, wait for server reboot completion in logs
    * This will run the portal image upgrade for the non-main db servers and reboot
6. Under 'Actions', select '**upgrade_version**', click 'RUN' button, and wait for completion in the logs
    * This will upgrade & start the portal version for all portal servers, starting with the mainDB.
7. Under 'Actions', select '**post_check**', click 'RUN' button, and wait for completion in the logs
    * This will run our post upgrade checks and clear out the ephemeral SSH keys.

**Functions**
- pre_check:
- multi_server_upload:
- unpack_images:
- upgrade_db_image:
- upgrade_non_db_images:
- upgrade_version:
- post_check:

.exe builds tested on Windows 10

**Upcoming Features**

- [x] Completion Timers
- [x] Disk space pre-checks
- [ ] Post-upgrade sanity checks
- [ ] Upload files once from client and distribute from mainDB.
- [ ] Download image and version files from remote URL to client machine
- [x] More server role distinctions
- [x] Support disparate root passwords via ephemeral SSH keys 
- [ ] Post upgrade file cleanup 

