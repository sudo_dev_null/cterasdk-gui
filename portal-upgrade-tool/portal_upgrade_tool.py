# -*- coding: utf-8 -*-

import wx
import wx.xrc

###########################################################################
## Class frameMain
###########################################################################

class frameMain ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"CTERA Portal Upgrade Utility", pos = wx.DefaultPosition, size = wx.Size( 870,777 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer3 = wx.BoxSizer( wx.VERTICAL )

		inputSizer = wx.FlexGridSizer( 10, 3, 5, 50 )
		inputSizer.SetFlexibleDirection( wx.BOTH )
		inputSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.portalLabel = wx.StaticText( self, wx.ID_ANY, u"Portal DNS or Application IP", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.portalLabel.Wrap( -1 )

		inputSizer.Add( self.portalLabel, 0, wx.ALL, 5 )

		self.address = wx.TextCtrl( self, wx.ID_ANY, u"192.168.1.151", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.address, 0, wx.ALL, 5 )

		self.blankText1 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.blankText1.Wrap( -1 )

		inputSizer.Add( self.blankText1, 0, wx.ALL, 5 )

		self.m_staticText29 = wx.StaticText( self, wx.ID_ANY, u"Portal Administrator User/Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText29.Wrap( -1 )

		inputSizer.Add( self.m_staticText29, 0, wx.ALL, 5 )

		self.user = wx.TextCtrl( self, wx.ID_ANY, u"cteraadmin", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.user, 0, wx.ALL, 5 )

		self.password = wx.TextCtrl( self, wx.ID_ANY, u"password", wx.DefaultPosition, wx.Size( 200,-1 ), wx.TE_PASSWORD )
		inputSizer.Add( self.password, 0, wx.ALL, 5 )

		self.m_staticText30 = wx.StaticText( self, wx.ID_ANY, u"SSH User/password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText30.Wrap( -1 )

		inputSizer.Add( self.m_staticText30, 0, wx.ALL, 5 )

		self.ssh_user = wx.TextCtrl( self, wx.ID_ANY, u"root", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		inputSizer.Add( self.ssh_user, 0, wx.ALL, 5 )

		self.ssh_pass = wx.TextCtrl( self, wx.ID_ANY, u"ctera321", wx.DefaultPosition, wx.Size( 200,-1 ), wx.TE_PASSWORD )
		inputSizer.Add( self.ssh_pass, 0, wx.ALL, 5 )


		bSizer3.Add( inputSizer, 1, wx.ALL|wx.EXPAND, 5 )

		fgSizer2 = wx.FlexGridSizer( 10, 2, 5, 60 )
		fgSizer2.SetFlexibleDirection( wx.BOTH )
		fgSizer2.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.m_staticText31 = wx.StaticText( self, wx.ID_ANY, u"Select Portal Servers:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText31.Wrap( -1 )

		fgSizer2.Add( self.m_staticText31, 0, wx.ALL, 5 )

		serversListBoxChoices = []
		self.serversListBox = wx.ListBox( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 475,200 ), serversListBoxChoices, wx.LB_EXTENDED )
		fgSizer2.Add( self.serversListBox, 0, wx.ALL, 5 )

		self.uploadFileLabel = wx.StaticText( self, wx.ID_ANY, u"Select the portal image file:", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		self.uploadFileLabel.Wrap( -1 )

		fgSizer2.Add( self.uploadFileLabel, 0, wx.ALL, 5 )

		self.imageUploadPicker = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.Size( 475,-1 ), wx.FLP_DEFAULT_STYLE )
		fgSizer2.Add( self.imageUploadPicker, 0, wx.ALL, 5 )

		self.uploadFileLabel1 = wx.StaticText( self, wx.ID_ANY, u"Select the portal version file:", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		self.uploadFileLabel1.Wrap( -1 )

		fgSizer2.Add( self.uploadFileLabel1, 0, wx.ALL, 5 )

		self.versionUploadPicker = wx.FilePickerCtrl( self, wx.ID_ANY, wx.EmptyString, u"Select a file", u"*.*", wx.DefaultPosition, wx.Size( 475,-1 ), wx.FLP_DEFAULT_STYLE )
		fgSizer2.Add( self.versionUploadPicker, 0, wx.ALL, 5 )

		self.uploadPathLabel = wx.StaticText( self, wx.ID_ANY, u"Target Upload Path:", wx.DefaultPosition, wx.Size( 200,-1 ), 0 )
		self.uploadPathLabel.Wrap( -1 )

		fgSizer2.Add( self.uploadPathLabel, 0, wx.ALL, 5 )

		self.uploadPath = wx.TextCtrl( self, wx.ID_ANY, u"/usr/local/lib/ctera/upgrade-6.0.780.16/", wx.DefaultPosition, wx.Size( 475,-1 ), 0 )
		fgSizer2.Add( self.uploadPath, 0, wx.ALL, 5 )

		self.progressLabel1 = wx.StaticText( self, wx.ID_ANY, u"Transfer Progress: ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.progressLabel1.Wrap( -1 )

		fgSizer2.Add( self.progressLabel1, 0, wx.ALL, 5 )

		self.gauge1 = wx.Gauge( self, wx.ID_ANY, 100, wx.DefaultPosition, wx.Size( 200,-1 ), wx.GA_HORIZONTAL )
		self.gauge1.SetValue( 0 )
		fgSizer2.Add( self.gauge1, 0, wx.ALL, 5 )


		bSizer3.Add( fgSizer2, 1, wx.EXPAND, 5 )

		commandSizer = wx.FlexGridSizer( 0, 3, 0, 0 )
		commandSizer.SetFlexibleDirection( wx.BOTH )
		commandSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )

		self.actionLabel = wx.StaticText( self, wx.ID_ANY, u"Actions: ", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.actionLabel.Wrap( -1 )

		commandSizer.Add( self.actionLabel, 0, wx.ALL, 5 )

		actionChoices = [ u"setup_auth", u"pre_check", u"multi_server_upload", u"unpack_image", u"upgrade_db_image", u"upgrade_non_db_images", u"upgrade_version", u"post_check" ]
		self.action = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( 300,-1 ), actionChoices, 0 )
		self.action.SetSelection( 0 )
		commandSizer.Add( self.action, 0, wx.ALL, 5 )

		self.loginButton = wx.Button( self, wx.ID_ANY, u"RUN", wx.DefaultPosition, wx.DefaultSize, 0 )
		commandSizer.Add( self.loginButton, 0, wx.ALL, 5 )


		bSizer3.Add( commandSizer, 1, wx.ALL|wx.EXPAND, 15 )


		self.SetSizer( bSizer3 )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.loginButton.Bind( wx.EVT_BUTTON, self.runScript )

	def __del__( self ):
		pass


	def runScript( self, event ):
		global isPortalUpgradable
		if(self.action.GetStringSelection() == "setup_auth"):
			setupAuthThread = threading.Thread(target=setup_auth,args=(self,))
			setupAuthThread.start()
		if(self.action.GetStringSelection() == "pre_check"):
			preCheckThread = threading.Thread(target=pre_check,args=(self,))
			preCheckThread.start()
		if(not isPortalUpgradable):
			config.logging.error("Portal not upgradable.  Check available disk space on servers.")
		if(self.action.GetStringSelection() == "multi_server_upload" and isPortalUpgradable):
			initAppThread = threading.Thread(target=multi_server_upload,args=(self,))
			initAppThread.start()

		if(self.action.GetStringSelection() == "unpack_image" and isPortalUpgradable):
			unpackImageThread = threading.Thread(target=unpack_image,args=(self,))
			unpackImageThread.start()

		if(self.action.GetStringSelection() == "upgrade_db_image" and isPortalUpgradable):			
			dbImageThread = threading.Thread(target=upgrade_db_image,args=(self,))
			dbImageThread.start()

		if(self.action.GetStringSelection() == "upgrade_non_db_images" and isPortalUpgradable):			
			nonDbImageThread = threading.Thread(target=upgrade_non_db_images,args=(self,))
			nonDbImageThread.start()
		
		if(self.action.GetStringSelection() == "upgrade_version" and isPortalUpgradable):
			upgradeVersionThread = threading.Thread(target=upgrade_version,args=(self,))
			upgradeVersionThread.start()

		if(self.action.GetStringSelection() == "post_check" and isPortalUpgradable):
			uploadThread = threading.Thread(target=post_check,args=(self,))
			uploadThread.start()
		event.Skip()

def setup_auth(self):
	key = RSA.generate(2048)
	private_key = key.export_key()
	file_out = open("portal_upgrade_key.pem", "wt")
	file_out.write(str(private_key, 'utf-8') + '\n')
	file_out.close()
	os.chmod("portal_upgrade_key.pem", 0o600)

	public_key = key.publickey().export_key(format="OpenSSH")
	file_out = open("portal_upgrade_key.pub", "wt")
	file_out.write(str(public_key, 'utf-8') + ' client@upgrade_tool' + '\n')
	#file_out.write("\n")
	file_out.close()		
	admin = GlobalAdmin(self.address.Value)
	admin.login(self.user.Value, self.password.Value)
	admin.whoami()
	admin.portals.browse_global_admin()
	servers = admin.servers.list_servers(include = ['name', 'defaultIpaddr', 'connected', 'isApplicationServer', 'mainDB', 'previewStatus', 'replicationSettings', 'role', 'imageVersion', 'runningVersion', 'serverString', 'rootDirPercentage', 'dataDirPercentage', 'isUpgradable'])	

	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(self.address.Value, username=self.ssh_user.Value, password=self.ssh_pass.Value)

	config.logging.warning('Start ephemeral public key upload')
	sftp = ssh.open_sftp()
	sftp.put('./portal_upgrade_key.pub', '/tmp/portal_upgrade_key.pub')
	sftp.close()
	for server in servers:
		scpCommand = "scp -o StrictHostKeyChecking=no -i /home/ctera/.ssh/id_rsa /tmp/portal_upgrade_key.pub ctera@" + str(server.defaultIpaddr) + ":/tmp/portal_upgrade_key.pub"
		config.logging.info("SSH Key scp command: " + str(scpCommand))

		setupMkdirCommand = 'sudo \'bash -c "mkdir -p /root/.ssh && cat >> /root/.ssh/authorized_keys" < /tmp/portal_upgrade_key.pub\''
		config.logging.info("SSH Key setupMkdir command: " + str(setupMkdirCommand))
		
		mkdirCommand = "ssh -i /home/ctera/.ssh/id_rsa ctera@" + str(server.defaultIpaddr) + " " + str(setupMkdirCommand)
		config.logging.info("SSH Key mkdir command: " + str(mkdirCommand))

		chmodDirCommand = "ssh -i /home/ctera/.ssh/id_rsa ctera@" + str(server.defaultIpaddr) + " 'sudo chmod 700 /root/.ssh/'"
		config.logging.info("SSH Key chmodDir command: " + str(chmodDirCommand))

		chmodKeysCommand = "ssh -i /home/ctera/.ssh/id_rsa ctera@" + str(server.defaultIpaddr) + " 'sudo chmod 600 /root/.ssh/authorized_keys'"
		config.logging.info("SSH Key chmodKeys command: " + str(chmodKeysCommand))

		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(scpCommand)
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			config.logging.info(line)
		for line in ssh_stderr:
			config.logging.info(line)

		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(mkdirCommand)
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			config.logging.info(line)
		for line in ssh_stderr:
			config.logging.info(line)
		
		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(chmodDirCommand)
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			config.logging.info(line)
		for line in ssh_stderr:
			config.logging.info(line)

		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(chmodKeysCommand)
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			config.logging.info(line)
		for line in ssh_stderr:
			config.logging.info(line)	
	ssh.close()

def pre_check(self):
	global isPortalUpgradable
	serverList.clear()
	self.serversListBox.Clear()
	config.logging.info('Pre-check started on ' + self.address.Value)
	
	admin = GlobalAdmin(self.address.Value)
	admin.login(self.user.Value,self.password.Value)
	admin.whoami()
	admin.portals.browse_global_admin()
	
	servers = admin.servers.list_servers(include = ['name', 'defaultIpaddr', 'connected', 'isApplicationServer', 'mainDB', 'previewStatus', 'replicationSettings', 'role', 'imageVersion', 'runningVersion', 'serverString', 'rootDirPercentage', 'dataDirPercentage', 'isUpgradable'])	
	rootDirCommand = "df -T /usr/local/lib/ctera| tail -1 | awk '{print $5}'"
	dataDirCommand = "df -T /| tail -1 | awk '{print $5}'"


	for server in servers:
		server.isUpgradable = False
		server.role = "other"
		if(server.isApplicationServer):
			server.role = "app"
		if(server.previewStatus == "Enabled"):
			server.role = "preview"
		if(server.replicationSettings is not None):
			server.role = "replicaDB"
		if(server.mainDB):
			server.role = "mainDB"

		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh status')
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			config.logging.info(line.rstrip('\n'))

		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('cat /etc/ctera/PORTAL_IMAGE_VERSION')
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			config.logging.info('Image version detected: ' + line.rstrip('\n'))
			server.imageVersion = line.rstrip('\n')

		# Check for available space on root dir
		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(rootDirCommand) 
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			server.rootDirAvailable = int(line.rstrip('\n'))
			config.logging.warning('Root partition space available for {a} is {b}'.format(a=server.name, b=server.rootDirAvailable))

		# Check for available space on $datadir
		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(dataDirCommand) 
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			server.dataDirAvailable = int(line.rstrip('\n'))
			config.logging.warning('datadir partition space available for {a} is {b}'.format(a=server.name, b=server.dataDirAvailable))
		
		server.serverString = str(server.defaultIpaddr + "/" + server.name + "/" + server.role + "/" + str(round(server.rootDirAvailable/1024000, 2)) + "G/" + str(round(server.dataDirAvailable/1024000, 2)) + "G/" + str(server.imageVersion) + "/" + str(server.runningVersion))
		
		#Check for apx. 2.5GB of available space on (slash) and 5GB on $datadir
		if(server.rootDirAvailable > 2500000):
			if(server.dataDirAvailable > 5000000):
				server.isUpgradable = True
				isPortalUpgradableList.append(True)
				config.logging.warning('Server has space for upgrade ' + server.serverString)

		if(not server.isUpgradable):
			isPortalUpgradableList.append(False)
			config.logging.warning('Cannot upgrade (check disk space) ' + server.serverString)

		config.logging.info('Found portal server: ' + server.serverString)
		self.serversListBox.Append(server.serverString)
		serverList.append(server)
		config.logging.warning('Server added to list ' + server.defaultIpaddr)
		config.logging.info('Pre-check complete on ' + server.serverString)

	#check if all servers are upgradeable
	if(False not in isPortalUpgradableList):
		isPortalUpgradable = True
	else:
		isPortalUpgradable = False
		
	config.logging.warning('isPortalUpgradable = ' + str(isPortalUpgradable))
	config.logging.warning('All pre-checks complete.' )
	return

def multi_server_upload(self):
	start_time = time.time()
	config.logging.warning('Uploading files started')
	imageFileName = PurePath(self.imageUploadPicker.GetPath()).name	
	config.logging.info('image name set to ' + imageFileName)
	versionFileName = PurePath(self.versionUploadPicker.GetPath()).name
	config.logging.info('version name set to ' + versionFileName)
	
	for server in serverList:
		#config.logging.info('Enter server upload loop')
		config.logging.info(server)
		config.logging.info('Start SSH for ' + server.serverString)
		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
		config.logging.info('SSH connected for ' + server.serverString)
		
		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('mkdir -p ' + self.uploadPath.Value + ';chown ctera:ctera ' + self.uploadPath.Value)
		config.logging.info('mkdir -p complete ' + server.serverString)
		ssh.close()
	
	for server in serverList:
		if(server.mainDB):
			config.logging.info('Start SSH for ' + server.serverString)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
			config.logging.info('SSH connected for ' + server.serverString)
				
			sftp = ssh.open_sftp()
			sftp.chdir(self.uploadPath.Value)
			config.logging.warning('Start image upload for ' + server.serverString)
			sftp.put(self.imageUploadPicker.GetPath(), str(self.uploadPath.Value + imageFileName), callback=printTransferProgress)
			config.logging.warning('Start version upload for ' + server.serverString)
			sftp.put(self.versionUploadPicker.GetPath(), str(self.uploadPath.Value + versionFileName))
			sftp.close()
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('chown -R ctera:ctera ' + self.uploadPath.Value)
			config.logging.info('chown complete ' + server.serverString)

			for server in serverList:
				if(not server.mainDB):
					imageScpCommand = "scp -o StrictHostKeyChecking=no -i /home/ctera/.ssh/id_rsa " + str(self.uploadPath.Value + imageFileName) + " ctera@" + str(server.defaultIpaddr) + ":" + str(self.uploadPath.Value + imageFileName)
					config.logging.info("SSH Key scp command: " + str(imageScpCommand))
					ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(imageScpCommand)
					ssh_stdout.channel.recv_exit_status()
					for line in ssh_stdout:
						config.logging.info(line)
					for line in ssh_stderr:
						config.logging.info(line)

					versionScpCommand = "scp -o StrictHostKeyChecking=no -i /home/ctera/.ssh/id_rsa " + str(self.uploadPath.Value + versionFileName) + " ctera@" + str(server.defaultIpaddr) + ":" + str(self.uploadPath.Value + versionFileName)
					config.logging.info("SSH Key scp command: " + str(versionScpCommand))
					ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(versionScpCommand)
					ssh_stdout.channel.recv_exit_status()
					for line in ssh_stdout:
						config.logging.info(line)
					for line in ssh_stderr:
						config.logging.info(line)
			ssh.close()

	elapsed_time = time.time() - start_time
	config.logging.warning('Uploads complete in ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_time)) )
	return

def unpack_image(self):
	config.logging.warning('Unpack image started')
	imageFileName = PurePath(self.imageUploadPicker.GetPath()).name	
	config.logging.info('Portal image name set to ' + imageFileName)

	for server in serverList:
		config.logging.info(server)
		unpackingThread = threading.Thread(target=unpack_image_thread,args=(self, server.defaultIpaddr, self.ssh_user.Value, self.ssh_pass.Value, self.uploadPath.Value, imageFileName, server.serverString))
		unpackingThread.start()
	config.logging.warning('All portal image unpack commands dispacthed.  Please wait about 5 minutes for completion.')
	return

def unpack_image_thread(self, defaultIpaddr, ssh_user, ssh_pass, uploadPath, imageFileName, serverString):
	start_time = time.time()
	config.logging.warning('Unpack image thread')
	config.logging.info('Start SSH for ' + serverString)
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(defaultIpaddr, username=ssh_user, key_filename='portal_upgrade_key.pem', password=ssh_pass)
	config.logging.info('SSH connected for ' + serverString)

	config.logging.warning('Portal image unpack starting ' + serverString)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('cd ' + uploadPath +'; tar -xvf ' + imageFileName)
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)
	elapsed_time = time.time() - start_time
	config.logging.warning('Portal image unpack complete for {a} in {b}'.format(a=serverString, b=time.strftime("%H:%M:%S", time.gmtime(elapsed_time))))
	return

def upgrade_db_image(self):
	start_time = time.time()
	config.logging.warning('Upgrade portal image started')
	imageFileName = PurePath(self.imageUploadPicker.GetPath()).name	
	imageFileStem = PurePath(PurePath(self.imageUploadPicker.GetPath()).stem).stem	
	config.logging.info('Portal image name set to ' + imageFileName)
	config.logging.info('Portal image stem set to ' + imageFileStem)

	for server in serverList:
		#server.serverString = str(server.defaultIpaddr + "/" + server.name + "/" + str(server.mainDB))
		config.logging.info('Enter stop services loop..... checking for application ' + server.serverString)
		if(server.isApplicationServer and not server.mainDB):
			config.logging.info('Start SSH for application stop services ' + server.serverString)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
			config.logging.info('SSH connected for ' + server.serverString)
			config.logging.warning('Stopping portal services ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh stop')
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)
	
	for server in serverList:
		print(server)
		config.logging.info('Enter upgrade loop')	
		if(server.mainDB):
			config.logging.info('Start SSH for main DB image upgrade ' + server.serverString)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
			config.logging.info('SSH connected for ' + server.serverString)
			config.logging.warning('Stopping portal services ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh stop')
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)

			config.logging.warning('Portal image upgrade starting. Please wait up to 10 minutes for completion.' + server.serverString)
			imageUpgradeCommand = "cd " + self.uploadPath.Value + imageFileStem + "; nohup ./install.sh -u"
			config.logging.info('image upgrade command = ' + imageUpgradeCommand)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(imageUpgradeCommand)
			#ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('cd ' + self.uploadPath.Value + imageFileStem + ';./install.sh -u')
			ssh_stdout.channel.recv_exit_status()
			elapsed_time = time.time() - start_time
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)
			config.logging.warning('Portal image upgrade complete for {a} in {b}'.format(a=server.serverString, b=time.strftime("%H:%M:%S", time.gmtime(elapsed_time))))
			config.logging.warning('Send reboot ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("/sbin/reboot -f > /dev/null 2>&1 &")
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)
			config.logging.warning('Reboot sent ' + server.serverString)
	return

def upgrade_non_db_images(self):
	config.logging.warning('Upgrade portal image for non-dbs started')
	imageFileName = PurePath(self.imageUploadPicker.GetPath()).name	
	imageFileStem = PurePath(PurePath(self.imageUploadPicker.GetPath()).stem).stem	
	config.logging.info('Portal image name set to ' + imageFileName)
	config.logging.info('Portal image stem set to ' + imageFileStem)
	
	for server in serverList:
		config.logging.warning(server)
		config.logging.info('Enter upgrade loop')	
		if(not server.mainDB):
			upgradeThread = threading.Thread(target=upgrade_server_thread,args=(self, self.uploadPath.Value, imageFileStem, server.defaultIpaddr, self.ssh_user.Value, self.ssh_pass.Value, server.serverString))
			upgradeThread.start()
	return

def upgrade_server_thread(self, uploadPath, imageFileStem, defaultIpaddr, ssh_user, ssh_pass, serverString):
	start_time = time.time()
	config.logging.info('Enter portal image upgrade thread ' + serverString)
	config.logging.info('Start SSH for image upgrade: ' + serverString)
	ssh = paramiko.SSHClient()
	ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	ssh.connect(defaultIpaddr, username=ssh_user, key_filename='portal_upgrade_key.pem', password=ssh_pass)
	config.logging.info('SSH connected for ' + serverString)
	config.logging.warning('Stopping portal services ' + serverString)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh stop')
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)
	config.logging.warning('Portal image upgrade starting. Please wait about 10 minutes for completion.' + serverString)
	imageUpgradeCommand = "cd " + self.uploadPath.Value + imageFileStem + "; nohup ./install.sh -u"
	config.logging.info('image upgrade command = ' + imageUpgradeCommand)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(imageUpgradeCommand)
	#ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('cd ' + uploadPath + imageFileStem + ';./install.sh -u')
	ssh_stdout.channel.recv_exit_status()
	elapsed_time = time.time() - start_time
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)
	config.logging.warning('Portal image upgrade complete for {a} in {b}'.format(a=serverString, b=time.strftime("%H:%M:%S", time.gmtime(elapsed_time))))
	config.logging.warning('Send reboot ' + serverString)
	ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("/sbin/reboot -f > /dev/null 2>&1 &")
	ssh_stdout.channel.recv_exit_status()
	for line in ssh_stdout:
		config.logging.info(line)
	for line in ssh_stderr:
		config.logging.info(line)
	config.logging.warning('Reboot sent ' + serverString)
	return

def upgrade_version(self):
	start_time = time.time()
	config.logging.warning('Upgrade portal version started')
	versionFileName = PurePath(self.versionUploadPicker.GetPath()).name	
	versionFileStem = PurePath(self.versionUploadPicker.GetPath()).stem	
	config.logging.info('Portal version name set to ' + versionFileName)
	config.logging.info('Portal version stem set to ' + versionFileStem)
#	config.logging.info("before server list loop")	
	
	for server in serverList:
		config.logging.info('Enter portal services shutdown loop..... checking for non-main db ' + server.serverString)	
		if(not server.mainDB):
			config.logging.warning('Start SSH for non-DB portal services shutdown ' + server.serverString)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
			config.logging.info('SSH connected for ' + server.serverString)
			config.logging.warning('Stopping portal services ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh stop')
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)	

	for server in serverList:
		config.logging.info('Enter version upgrade loop..... checking for main db ' + server.serverString)
		if(server.mainDB):
			config.logging.info('Start SSH for main DB version upgrade ' + server.serverString)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
			config.logging.info('SSH connected for ' + server.serverString)
			config.logging.warning('Stopping portal services ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh stop')
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)
			config.logging.warning('Portal version upgrade starting ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('cd ' + self.uploadPath.Value + ';/usr/local/ctera/bin/ctera-portal-manage.sh upgrade ' + versionFileName)
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)
			config.logging.warning('Portal version upgrade complete ' + server.serverString)
	
	for server in serverList:
		config.logging.info('Enter version upgrade loop..... checking for non-main db ' + server.serverString)
		if(not server.mainDB):
			config.logging.info('Start SSH for non-DB version upgrade ' + server.serverString)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
			config.logging.info('SSH connected for ' + server.serverString)
			config.logging.warning('Stopping portal services ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh stop')
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)

			config.logging.warning('Portal version upgrade starting ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('cd ' + self.uploadPath.Value + ';/usr/local/ctera/bin/ctera-portal-manage.sh upgrade ' + versionFileName)
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)
			config.logging.warning('Portal version upgrade complete ' + server.serverString)

	for server in serverList:
		config.logging.info('Enter portal service start loop..... checking for main db ' + server.serverString)
		if(server.mainDB):
			config.logging.info('Start SSH for DB portal services start ' + server.serverString)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
			config.logging.info('SSH connected for ' + server.serverString)
			config.logging.warning('Starting portal services ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh start')
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)
			config.logging.warning('Portal services started ' + server.serverString)					

	for server in serverList:
		config.logging.info('Enter portal service start loop..... checking for non-main db ' + server.serverString)	
		if(not server.mainDB):
			config.logging.warning('Start SSH for non-DB portal services start ' + server.serverString)
			ssh = paramiko.SSHClient()
			ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
			config.logging.info('SSH connected for ' + server.serverString)
			config.logging.warning('Starting portal services ' + server.serverString)
			ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('/usr/local/ctera/bin/ctera-portal-manage.sh start')
			ssh_stdout.channel.recv_exit_status()
			for line in ssh_stdout:
				config.logging.info(line)
			for line in ssh_stderr:
				config.logging.info(line)
			config.logging.warning('Portal services started ' + server.serverString)
	elapsed_time = time.time() - start_time
	config.logging.warning('All portal versions upgraded in {a}'.format(a=time.strftime("%H:%M:%S", time.gmtime(elapsed_time))))
	return

def post_check(self):
	start_time = time.time()
	config.logging.warning('Post check started')
	for server in serverList:
		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(server.defaultIpaddr, username=self.ssh_user.Value, key_filename='portal_upgrade_key.pem', password=self.ssh_pass.Value)
		config.logging.info('SSH connected for ' + server.serverString)
		config.logging.info('Cleanup SSH authorized_keys on: ' + server.serverString)
		ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('sed -i.bak \'/client@upgrade_tool/d\' /root/.ssh/authorized_keys')
		ssh_stdout.channel.recv_exit_status()
		for line in ssh_stdout:
			config.logging.info(line)
		for line in ssh_stderr:
			config.logging.info(line)

	#sed -i.bak '/client@upgrade_tool/d' ~/.ssh/authorized_keys
	elapsed_time = time.time() - start_time
	config.logging.warning('Post check complete in {a}'.format(a=time.strftime("%H:%M:%S", time.gmtime(elapsed_time))))	

def printTransferProgress(transferred, toBeTransferred):
	frame.gauge1.SetRange(toBeTransferred)
	frame.gauge1.SetValue(transferred)
	if (transferred % 150000 == 0):
		transferred = round(transferred / 1000000, 2)
		toBeTransferred = round(toBeTransferred / 1000000, 2)
		config.logging.warning("Transferred: {0}MB\tOut of: {1}MB".format(transferred, toBeTransferred))
	return
		
import os
os.environ['CTERASDK_LOG_FILE'] = 'portal_upgrade.log'   #Log to local file
from cterasdk import *
from datetime import datetime
from pathlib import PurePath
import paramiko
import sys
from sys import platform
import subprocess
import threading
import time
from Crypto.PublicKey import RSA

config.logging.info('Agent Started')
config.http['ssl'] = 'Trust'

#If running Windows, open a PS window and tail our log
if (platform == "win32"):
	subprocess.Popen('powershell.exe Get-Content ./portal_upgrade.log –Wait')

servers = []
serverList = []
isPortalUpgradableList = []
global isPortalUpgradable
isPortalUpgradable = False
#print(os.environ.get('CTERASDK_LOG_FILE'))
#Main Program
app = wx.App()
frame = frameMain(None)
frame.Show()
app.MainLoop()
